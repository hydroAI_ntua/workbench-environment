# README #


### What is this repository for? ###

This is the Dockerfile for Artenet's Workbench environment.

### How do I get set up? ###

In order to set up and run the workbench you need a working Docker installation on your system. 
For more details visit https://www.docker.com/products/docker-desktop

Once the Docker Daemon is running on your system you can simply run the workbench by running on a terminal
`docker run -p 8888:8888 eftaxiask/artenet_wb:workbench-cpu`. 

Once the image is pulled you can click the link that appears on the terminal or 
browse http://localhost:8888 to run jupyter lab. 



### Who do I talk to? ###

Kostas Eftaxias <keftaxias@mail.ntua.gr>