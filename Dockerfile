# Dockerfile for building the workbench environment with TF-Agents and ML packages
#
#
FROM tensorflow/tensorflow:2.1.0-py3 as base
LABEL maintainer="Kostas Eftaxias <keftaxias@mail.ntua.gr>"
RUN pip install tf-agents
# See http://bugs.python.org/issue19846
ENV LANG C.UTF-8
RUN apt-get update
# Resolves tzdata asking for the timezone. python-opencv depends on tzdata.
RUN ln -fs /usr/share/zoneinfo/Europe/Athens /etc/localtime
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata
RUN dpkg-reconfigure --frontend noninteractive tzdata
# Pick up some TF dependencies
RUN apt-get install -y --no-install-recommends \
      cmake \
      zlib1g-dev \
      libpng-dev \
      lsb-core \
      vim \
      less \
      git \
      ca-certificates \
      wget \
      zip \
      xvfb \
      freeglut3-dev \
      ffmpeg \
      python-opencv
RUN pip install --upgrade pip setuptools
RUN pip install wheel \
      opencv-python \
      gym \
      atari-py \
      pybullet \
      gin-config \
      virtualenv \
      matplot \
      absl-py \
      mock \
      imageio \
      PILLOW \
      pyglet \
      pyvirtualdisplay \
      pandas \
      scikit-learn \
      seaborn

######################################################
# Jupyter notebook ipykernel downgrade is needed due to
# https://github.com/ipython/ipykernel/issues/422
######################################################
RUN pip install jupyter jupyterlab
RUN pip install ipykernel==5.1.1
RUN pip freeze
RUN jupyter serverextension enable --py jupyterlab
WORKDIR /tf/notebooks
CMD ["jupyter", "lab", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]